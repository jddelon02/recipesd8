<?php

/**
 * @file
 * Contains plant_info_entity.page.inc.
 *
 * Page callback for Plant Info entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Plant Info templates.
 *
 * Default template: plant_info_entity.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_plant_info_entity(array &$variables) {
  // Fetch PlantInfoEntity Entity Object.
  $plant_info_entity = $variables['elements']['#plant_info_entity'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
