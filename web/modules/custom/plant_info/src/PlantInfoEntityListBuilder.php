<?php

namespace Drupal\plant_info;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Plant Info entities.
 *
 * @ingroup plant_info
 */
class PlantInfoEntityListBuilder extends EntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Plant Info ID');
    $header['name'] = $this->t('Name');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var \Drupal\plant_info\Entity\PlantInfoEntity $entity */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.plant_info_entity.edit_form',
      ['plant_info_entity' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
