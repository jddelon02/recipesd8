<?php

namespace Drupal\plant_info\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Plant Info type entities.
 */
interface PlantInfoEntityTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
