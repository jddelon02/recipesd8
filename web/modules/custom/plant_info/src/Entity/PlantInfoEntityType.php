<?php

namespace Drupal\plant_info\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Plant Info type entity.
 *
 * @ConfigEntityType(
 *   id = "plant_info_entity_type",
 *   label = @Translation("Plant Info type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\plant_info\PlantInfoEntityTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\plant_info\Form\PlantInfoEntityTypeForm",
 *       "edit" = "Drupal\plant_info\Form\PlantInfoEntityTypeForm",
 *       "delete" = "Drupal\plant_info\Form\PlantInfoEntityTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\plant_info\PlantInfoEntityTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "plant_info_entity_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "plant_info_entity",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/plant_info_entity_type/{plant_info_entity_type}",
 *     "add-form" = "/admin/structure/plant_info_entity_type/add",
 *     "edit-form" = "/admin/structure/plant_info_entity_type/{plant_info_entity_type}/edit",
 *     "delete-form" = "/admin/structure/plant_info_entity_type/{plant_info_entity_type}/delete",
 *     "collection" = "/admin/structure/plant_info_entity_type"
 *   }
 * )
 */
class PlantInfoEntityType extends ConfigEntityBundleBase implements PlantInfoEntityTypeInterface {

  /**
   * The Plant Info type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Plant Info type label.
   *
   * @var string
   */
  protected $label;

}
