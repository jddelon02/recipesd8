<?php

namespace Drupal\plant_info\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Plant Info entities.
 *
 * @ingroup plant_info
 */
interface PlantInfoEntityInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Plant Info name.
   *
   * @return string
   *   Name of the Plant Info.
   */
  public function getName();

  /**
   * Sets the Plant Info name.
   *
   * @param string $name
   *   The Plant Info name.
   *
   * @return \Drupal\plant_info\Entity\PlantInfoEntityInterface
   *   The called Plant Info entity.
   */
  public function setName($name);

  /**
   * Gets the Plant Info creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Plant Info.
   */
  public function getCreatedTime();

  /**
   * Sets the Plant Info creation timestamp.
   *
   * @param int $timestamp
   *   The Plant Info creation timestamp.
   *
   * @return \Drupal\plant_info\Entity\PlantInfoEntityInterface
   *   The called Plant Info entity.
   */
  public function setCreatedTime($timestamp);

}
