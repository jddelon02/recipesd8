<?php

namespace Drupal\plant_info;

use Drupal\content_translation\ContentTranslationHandler;

/**
 * Defines the translation handler for plant_info_entity.
 */
class PlantInfoEntityTranslationHandler extends ContentTranslationHandler {

  // Override here the needed methods from ContentTranslationHandler.
}
