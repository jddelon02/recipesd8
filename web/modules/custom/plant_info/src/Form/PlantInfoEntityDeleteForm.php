<?php

namespace Drupal\plant_info\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Plant Info entities.
 *
 * @ingroup plant_info
 */
class PlantInfoEntityDeleteForm extends ContentEntityDeleteForm {


}
