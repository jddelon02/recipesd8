<?php

namespace Drupal\plant_info\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class PlantInfoEntityTypeForm.
 */
class PlantInfoEntityTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $plant_info_entity_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $plant_info_entity_type->label(),
      '#description' => $this->t("Label for the Plant Info type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $plant_info_entity_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\plant_info\Entity\PlantInfoEntityType::load',
      ],
      '#disabled' => !$plant_info_entity_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $plant_info_entity_type = $this->entity;
    $status = $plant_info_entity_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Plant Info type.', [
          '%label' => $plant_info_entity_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Plant Info type.', [
          '%label' => $plant_info_entity_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($plant_info_entity_type->toUrl('collection'));
  }

}
