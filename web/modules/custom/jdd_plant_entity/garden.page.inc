<?php

/**
 * @file
 * Contains garden.page.inc.
 *
 * Page callback for Garden entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Garden templates.
 *
 * Default template: garden.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_garden(array &$variables) {
  // Fetch garden Entity Object.
  $garden = $variables['elements']['#garden'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
