<?php

namespace Drupal\jdd_plant_entity\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Garden entities.
 */
class gardenViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.
    return $data;
  }

}
