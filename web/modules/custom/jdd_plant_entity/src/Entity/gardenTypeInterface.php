<?php

namespace Drupal\jdd_plant_entity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface for defining Garden type entities.
 */
interface gardenTypeInterface extends ConfigEntityInterface {

  // Add get/set methods for your configuration properties here.
}
