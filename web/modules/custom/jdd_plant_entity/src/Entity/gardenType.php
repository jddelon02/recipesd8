<?php

namespace Drupal\jdd_plant_entity\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the Garden type entity.
 *
 * @ConfigEntityType(
 *   id = "garden_type",
 *   label = @Translation("Garden type"),
 *   handlers = {
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\jdd_plant_entity\gardenTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\jdd_plant_entity\Form\gardenTypeForm",
 *       "edit" = "Drupal\jdd_plant_entity\Form\gardenTypeForm",
 *       "delete" = "Drupal\jdd_plant_entity\Form\gardenTypeDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\jdd_plant_entity\gardenTypeHtmlRouteProvider",
 *     },
 *   },
 *   config_prefix = "garden_type",
 *   admin_permission = "administer site configuration",
 *   bundle_of = "garden",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/garden_type/{garden_type}",
 *     "add-form" = "/admin/structure/garden_type/add",
 *     "edit-form" = "/admin/structure/garden_type/{garden_type}/edit",
 *     "delete-form" = "/admin/structure/garden_type/{garden_type}/delete",
 *     "collection" = "/admin/structure/garden_type"
 *   }
 * )
 */
class gardenType extends ConfigEntityBundleBase implements gardenTypeInterface {

  /**
   * The Garden type ID.
   *
   * @var string
   */
  protected $id;

  /**
   * The Garden type label.
   *
   * @var string
   */
  protected $label;

}
