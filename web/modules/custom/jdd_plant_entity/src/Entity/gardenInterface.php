<?php

namespace Drupal\jdd_plant_entity\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Garden entities.
 *
 * @ingroup jdd_plant_entity
 */
interface gardenInterface extends ContentEntityInterface, EntityChangedInterface, EntityPublishedInterface, EntityOwnerInterface {

  /**
   * Add get/set methods for your configuration properties here.
   */

  /**
   * Gets the Garden name.
   *
   * @return string
   *   Name of the Garden.
   */
  public function getName();

  /**
   * Sets the Garden name.
   *
   * @param string $name
   *   The Garden name.
   *
   * @return \Drupal\jdd_plant_entity\Entity\gardenInterface
   *   The called Garden entity.
   */
  public function setName($name);

  /**
   * Gets the Garden creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Garden.
   */
  public function getCreatedTime();

  /**
   * Sets the Garden creation timestamp.
   *
   * @param int $timestamp
   *   The Garden creation timestamp.
   *
   * @return \Drupal\jdd_plant_entity\Entity\gardenInterface
   *   The called Garden entity.
   */
  public function setCreatedTime($timestamp);

}
