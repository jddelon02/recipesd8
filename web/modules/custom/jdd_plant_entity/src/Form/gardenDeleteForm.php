<?php

namespace Drupal\jdd_plant_entity\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Garden entities.
 *
 * @ingroup jdd_plant_entity
 */
class gardenDeleteForm extends ContentEntityDeleteForm {


}
