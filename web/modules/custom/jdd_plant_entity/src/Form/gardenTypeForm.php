<?php

namespace Drupal\jdd_plant_entity\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class gardenTypeForm.
 */
class gardenTypeForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $garden_type = $this->entity;
    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $garden_type->label(),
      '#description' => $this->t("Label for the Garden type."),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $garden_type->id(),
      '#machine_name' => [
        'exists' => '\Drupal\jdd_plant_entity\Entity\gardenType::load',
      ],
      '#disabled' => !$garden_type->isNew(),
    ];

    /* You will need additional form elements for your custom properties. */

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $garden_type = $this->entity;
    $status = $garden_type->save();

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addMessage($this->t('Created the %label Garden type.', [
          '%label' => $garden_type->label(),
        ]));
        break;

      default:
        $this->messenger()->addMessage($this->t('Saved the %label Garden type.', [
          '%label' => $garden_type->label(),
        ]));
    }
    $form_state->setRedirectUrl($garden_type->toUrl('collection'));
  }

}
