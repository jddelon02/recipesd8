"use strict";

// Load plugins

const cssnano = require("cssnano");
const gulp = require("gulp");
const gulpIf = require('gulp-if');
const eslint = require("gulp-eslint");
const sass = require("gulp-sass");
const autoprefixer = require("autoprefixer");
const sourcemaps = require('gulp-sourcemaps');
const imagemin = require("gulp-imagemin");
const pngquant = require('imagemin-pngquant');
const del = require("del");
const newer = require("gulp-newer");
const plumber = require("gulp-plumber");
const postcss = require("gulp-postcss");
const rename = require("gulp-rename");

// Clean assets
function clean() {
  return del(["./css/", "./js/", "./images/", "./src/css_unmin/"]);
}

// Optimize Images
function images() {
  return gulp
    .src('./src/img/*')
    .pipe(
      imagemin([
        imagemin.gifsicle({ interlaced: true }),
        imagemin.jpegtran({ progressive: true }),
        imagemin.optipng({ optimizationLevel: 5 }),
        imagemin.svgo({
          plugins: [
            {
              removeViewBox: false,
              collapseGroups: true
            }
          ]
        })
      ])
    )
    .pipe(gulp.dest("./images"));
}

// CSS task
function css() {
  return gulp
    .src('./src/sass/**/*.scss')
    .pipe(plumber())
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: "expanded" }))
    .pipe(gulp.dest("./src/css_unmin/"))
    .pipe(rename({ suffix: ".min" }))
    .pipe(sass().on('error', sass.logError))
    .pipe(postcss([autoprefixer(), cssnano()]))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest("./css/"))
}

// Lint scripts
function scriptsLint() {
  return gulp
    .src(["./src/js/**/*"])
    .pipe(plumber())
    .pipe(eslint({
  			"extends": "eslint:recommended",
        "envs": [
            'browser'
        ],
  			"globals": [
    			"Drupal",
    			"drupalSettings",
   				"drupalTranslations",
    			"domready",
    			"jQuery",
   				"_",
    			"matchMedia",
    			"Backbone",
    			"Modernizr",
    			"CKEDITOR"
        ],
 			  "rules": {
    			// Errors.
    			"array-bracket-spacing": [2, "never"],
    			"block-scoped-var": 2,
   			  "brace-style": [2, "stroustrup", {"allowSingleLine": true}],
    			"comma-dangle": [2, "never"],
    			"comma-spacing": 2,
    			"comma-style": [2, "last"],
    			"computed-property-spacing": [2, "never"],
    			"curly": [2, "all"],
    			"eol-last": 2,
    			"eqeqeq": [2, "smart"],
    			"guard-for-in": 2,
    			"indent": [2, 2, {"SwitchCase": 1}],
   			  "key-spacing": [2, {"beforeColon": false, "afterColon": true}],
    			"linebreak-style": [2, "unix"],
    			"lines-around-comment": [2, {"beforeBlockComment": true, "afterBlockComment": false}],
    			"new-parens": 2,
    			"no-array-constructor": 2,
    			"no-caller": 2,
    			"no-catch-shadow": 2,
    			"no-empty-label": 2,
    			"no-eval": 2,
    			"no-extend-native": 2,
    			"no-extra-bind": 2,
    			"no-extra-parens": [2, "functions"],
    			"no-implied-eval": 2,
    			"no-iterator": 2,
    			"no-label-var": 2,
    			"no-labels": 2,
    			"no-lone-blocks": 2,
    			"no-loop-func": 2,
    			"no-multi-spaces": 2,
    			"no-multi-str": 2,
    			"no-native-reassign": 2,
    			"no-nested-ternary": 2,
    			"no-new-func": 2,
    			"no-new-object": 2,
   			  "no-new-wrappers": 2,
    			"no-octal-escape": 2,
    			"no-process-exit": 2,
    			"no-proto": 2,
    			"no-return-assign": 2,
    			"no-script-url": 2,
    			"no-sequences": 2,
    			"no-shadow-restricted-names": 2,
    			"no-spaced-func": 2,
    			"no-trailing-spaces": 2,
    			"no-undef-init": 2,
    			"no-undefined": 2,
    			"no-unused-expressions": 2,
    			"no-unused-vars": [2, {"vars": "all", "args": "none"}],
   			  "no-with": 2,
    			"object-curly-spacing": [2, "never"],
    			"one-var": [2, "never"],
    			"quote-props": [2, "consistent-as-needed"],
    			"semi": [2, "always"],
    			"semi-spacing": [2, {"before": false, "after": true}],
    			"space-after-keywords": [2, "always"],
    			"space-before-blocks": [2, "always"],
    			"space-before-function-paren": [2, {"anonymous": "always", "named": "never"}],
    			"space-in-parens": [2, "never"],
    			"space-infix-ops": 2,
    			"space-return-throw-case": 2,
    			"space-unary-ops": [2, { "words": true, "nonwords": false }],
    			"spaced-comment": [2, "always"],
    			"strict": 2,
    			"yoda": [2, "never"],
    			// Warnings.
    			"max-nested-callbacks": [1, 3],
    			"valid-jsdoc": [1, {
      			"prefer": {
       		 		"returns": "return",
        			"property": "prop"
      			  },
     	 		  "requireReturn": false
    			  }]
 	 		    },
 	 		  fix: true
    	  }
      )
    )
    .pipe(eslint.format())
    .pipe(eslint.failAfterError())
    .pipe(plumber())
    .pipe(gulp.dest("./js/"))
}

// Watch files
function watchFiles() {
  gulp.watch("./src/sass/**/*", css);
  gulp.watch("./src/js/**/*", scriptsLint);
  gulp.watch("./src/img/**/*", images);
}

// define complex tasks
const js = gulp.series(scriptsLint);
const build = gulp.series(clean, gulp.parallel(css, images, scriptsLint));
const watch = gulp.series(watchFiles);

// export tasks
exports.images = images;
exports.css = css;
exports.js = js;
exports.clean = clean;
exports.watch = watch;
exports.default = build;
