jQuery(function ($) {

  $('.dropdown-toggle').click(function () {
    'use strict';
    var location = $(this).attr('href');
    window.location.href = location;
    return false;
  });

});
