<?php

// @codingStandardsIgnoreFile

/**
 * @file
 * Local development override configuration feature.
 *
 * To activate this feature, copy and rename it such that its path plus
 * filename is 'sites/default/settings.local.php'. Then, go to the bottom of
 * 'sites/default/settings.php' and uncomment the commented lines that mention
 * 'settings.local.php'.
 *
 * If you are using a site name in the path, such as 'sites/example.com', copy
 * this file to 'sites/example.com/settings.local.php', and uncomment the lines
 * at the bottom of 'sites/example.com/settings.php'.
 */

/**
 * Salt for one-time login links, cancel links, form tokens, etc.
 */
$settings['hash_salt'] = 'Emfu/JJLCMYCAwIP9hiia91Tgnv8Wu+PhmLA63KlcII=';

// Local development configuration.
if (!defined('PANTHEON_ENVIRONMENT')) {
  // Database.
  $databases['default']['default'] = array (
    'database' => 'burgoyne_blue',
    'username' => 'burgoyne_blue',
    'password' => '24cvjBJFPGs5',
    'prefix' => '',
    'host' => '127.0.0.1',
    'port' => '3306',
    'namespace' => 'Drupal\\Core\\Database\\Driver\\mysql',
    'driver' => 'mysql',
  );
}
/**
 * Assertions.
 *
 * The Drupal project primarily uses runtime assertions to enforce the
 * expectations of the API by failing when incorrect calls are made by code
 * under development.
 *
 * @see http://php.net/assert
 * @see https://www.drupal.org/node/2492225
 *
 * If you are using PHP 7.0 it is strongly recommended that you set
 * zend.assertions=1 in the PHP.ini file (It cannot be changed from .htaccess
 * or runtime) on development machines and to 0 in production.
 *
 * @see https://wiki.php.net/rfc/expectations
 */
assert_options(ASSERT_ACTIVE, TRUE);
\Drupal\Component\Assertion\Handle::register();

/**
 * Enable local development services.
 */
$settings['container_yamls'][] = DRUPAL_ROOT . '/sites/development.services.yml';

/**
 * Show all error messages, with backtrace information.
 *
 * In case the error level could not be fetched from the database, as for
 * example the database connection failed, we rely only on this value.
 */
$config['system.logging']['error_level'] = 'verbose';

$config['system.performance']['cache']['page']['use_internal'] = TRUE;
$config['system.performance']['css']['preprocess'] = TRUE;
$config['system.performance']['css']['gzip'] = TRUE;
$config['system.performance']['js']['preprocess'] = TRUE;
$config['system.performance']['js']['gzip'] = TRUE;
$config['system.performance']['response']['gzip'] = TRUE;
$config['views.settings']['ui']['show']['sql_query']['enabled'] = FALSE;
$config['views.settings']['ui']['show']['performance_statistics'] = FALSE;
$config['system.logging']['error_level'] = 'none';

// Live Mail and Site settings
$config['system.site']['mail'] = 'chefjeremy@delongaz.com';
$config['system.site']['name'] = 'Burgoyne Blue';
//
//
// // Sendgrid setup.
// $config['sendgrid_integration.settings']['apikey'] = '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';
// $config['sendgrid_integration.settings']['test_defaults']['to'] = 'admin@example.com';
// $config['sendgrid_integration.settings']['test_defaults']['subject'] = 'Test Email from MA SendGrid Module';
// $config['sendgrid_integration.settings']['test_defaults']['body']['value'] = 'Test Message for MA SendGrid.';
// $config['sendgrid_integration.settings']['test_defaults']['body']['format'] = 'plain_text';
// $config['sendgrid_integration.settings']['test_defaults']['from_name'] = '';
// $config['sendgrid_integration.settings']['test_defaults']['to_name'] = '';
// $config['sendgrid_integration.settings']['test_defaults']['reply_to'] = '';
//
// // Mail Safety settings..
// $config['mail_safety.settings']['enabled'] = FALSE;
// $config['mail_safety.settings']['send_mail_to_dashboard'] = FALSE;

// Theme rebuild on every page load.
$config['devel.settings']['rebuild_theme'] = FALSE;

// // Google Analytics
// $config['google_analytics.settings']['account'] = 'UA-5674777-2';
